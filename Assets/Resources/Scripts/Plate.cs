using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour
{
    public int force;
    public int plateType = 0;
    public bool isActive;
    public Block activator;
    public List<Vector2> waypoints;
    private LineRenderer lr;

    void Awake()
    {
        if (plateType == 0)
        {
            Block b = gameObject.AddComponent<Block>();
            b.mass = 999;
            b.type = "Plate";
            b.ghostPass = true;
            b.blockPass = true;
        }
        isActive = false;
        lr = GetComponent<LineRenderer>();
        lr.material.SetColor("_Color", isActive ? Color.green : Color.red);
        lr.sortingOrder = isActive ? -2 : -1;
        lr.positionCount = 2 + waypoints.Count;
        lr.SetPosition(0, transform.position + new Vector3(0, -0.25f, 0));
        int i = 1;
        foreach (Vector2 point in waypoints)
        {
            lr.SetPosition(i, point + new Vector2(0, -0.25f));
            i++;
        }
        lr.SetPosition(lr.positionCount - 1, activator.transform.position + new Vector3(0, -0.25f, 0));
    }

    public bool ActivatePlate(List<Block> blocks)
    {
        foreach (Block block in blocks)
        {
            if (block.type != "Plate" && block.type != "Door"  && block.type != "Energy Field" && block.transform.position == transform.position)
            {
                if (block.mass >= force)
                {
                    return Switch(true);
                }
            }
        }
        return Switch(false);
    }

    public bool Switch(bool newActive)
    {
        if (isActive != newActive)
        {
            isActive = !isActive;
            AudioManager.GetInstance().PlayClip(isActive ? "platedown" : "plateup");
            if (plateType == 0)
            {
                Sprite plateSprite = Resources.Load<Sprite>("Sprites/plates/plate_" + force + "_" + (isActive ? 2 : 1));
                gameObject.GetComponent<SpriteRenderer>().sprite = plateSprite;
            }
            else if (plateType == 1)
            {
                gameObject.GetComponent<SpriteRenderer>().material.SetFloat("_Multiplier", isActive ? 1 : 0);
            }

            lr.material.SetColor("_Color", isActive ? Color.green : Color.red);
            lr.sortingOrder = isActive ? -2 : -1;

            activator.Activation(isActive);
        }
        return isActive;
    }

    public bool SameAs(Plate other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (force == other.force);
        result &= (isActive == other.isActive);
        result &= (transform.position == other.transform.position);
        return result;
    }

    public bool SameAs(PlateState other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (force == other.force);
        result &= (isActive == other.isActive);
        result &= (transform.position == other.position);
        return result;
    }
    public void rewindTo(PlateState other)
    {
        transform.position = other.position;
        force = other.force;
        Switch(other.isActive);
    }
}

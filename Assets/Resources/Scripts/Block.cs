using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Block : MonoBehaviour
{
    public string type;
    public int mass;
    public bool ghostPass;
    public bool blockPass;

    private void Start()
    {
        Sprite sprite;
        if (type == "Box")
        {
            sprite = Resources.Load<Sprite>("Sprites/box/box_" + mass + "_" + Random.Range(1, 3));
            if (sprite != null)
            {
                GetComponent<SpriteRenderer>().sprite = sprite;
            }
        }
        else if (type == "Energy Field")
        {
            sprite = Resources.Load<Sprite>("Sprites/EnergyField");
            if (sprite != null)
            {
                GetComponent<SpriteRenderer>().sprite = sprite;
                GetComponent<SpriteRenderer>().sharedMaterial.SetFloat("_Multiplier", 3f);
            }
        }
        else if (type == "Plate")
        {
            sprite = Resources.Load<Sprite>("Sprites/plates/plate_" + GetComponent<Plate>().force + "_1");
            if (sprite != null)
            {
                GetComponent<SpriteRenderer>().sprite = sprite;
            }
        }
    }

    public bool CanMove(Vector3 direction, List<Block> blocks, Tilemap walls, int power, int prevMass)
    {
        if (power < prevMass + mass)
        {
            return false;
        }
        Vector3 newPosition = transform.position + direction;
        if(walls.HasTile(Vector3Int.FloorToInt(newPosition) + new Vector3Int(-1, -1, 0)))
        {
            return false;
        }
        foreach (Block block in blocks)
        {
            if (newPosition == block.transform.position)
            {
                if(block.blockPass)
                {
                    continue;
                }
                return false;
                //return block.CanMove(direction, blocks, walls, power, prevMass + mass);
            }
        }
        return true;
    }

    public bool Move(Vector3 direction, List<Block> blocks, Tilemap walls, int power, int prevMass)
    {
        if (CanMove(direction, blocks, walls, power, prevMass))
        {
            AudioManager.GetInstance().PlayClip("box", .3f, 1f);
            transform.position += direction;
            return true;
            //return Move(direction, blocks, walls, power, prevMass + mass);
        }
        return false;
    }

    public bool SameAs(Block other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (type == other.type);
        result &= (mass == other.mass);
        result &= (ghostPass == other.ghostPass);
        result &= (blockPass == other.blockPass);
        result &= (transform.position == other.transform.position);
        return result;
    }

    public bool SameAs(BlockState other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (type == other.type);
        result &= (mass == other.mass);
        result &= (ghostPass == other.ghostPass);
        result &= (blockPass == other.droidPass);
        result &= (transform.position == other.position);
        return result;
    }
    public void rewindTo(BlockState other)
    {
        transform.position = other.position;
        mass = other.mass;
    }

    public void Activation(bool isActive)
    {
        // activate/deactivate doors and energy fields
        if (type == "Door")
        {
            StartCoroutine(DoorActivation(isActive));
        }
        else if (type == "Energy Field")
        {
            ghostPass = isActive;
            blockPass = true;
            GetComponent<SpriteRenderer>().material.SetFloat("_Multiplier", isActive ? .5f : 3f);
        }
    }

    private IEnumerator DoorActivation(bool isActive)
    {
        while (!isActive && IsDoorBlocked())
        {
            yield return null;
        }
        ghostPass = isActive;
        blockPass = isActive;
        Sprite doorSprite = Resources.Load<Sprite>("Sprites/door/door" + (isActive ? 2 : 1));
        AudioManager.GetInstance().PlayClip("door");
        gameObject.GetComponent<SpriteRenderer>().sprite = doorSprite;
    }

    private bool IsDoorBlocked()
    {
        foreach(Block block in Player.GetInstance().blocks)
        {
            if ((block.type == "Droid" || block.type == "Box") && (block.transform.position == transform.position))
            {
                return true;
            }
        }
        return false;
    }

    public List<string> GetFullInfo()
    {
        List<string> info = new List<string>();
        info.Add("Block");
        string blockType;
        switch (type)
        {
            case ("PowerPlate"):
                blockType = "Power station";
                break;
            case ("PowerBox"):
                blockType = "Generator";
                break;
            default:
                blockType = type;
                break;
        }
        info.Add(blockType);
        string power;
        switch (type)
        {
            case ("Energy Field"):
                power = ghostPass ? "0" : "1";
                break;
            case ("Door"):
                power = ghostPass && blockPass ? "0" : "1";
                break;
            case ("Plate"):
                power = GetComponent<Plate>().isActive ? "1" : "0";
                break;
            case ("PowerPlate"):
                power = GetComponent<Plate>().isActive ? "1" : "0";
                break;
            case ("PowerBox"):
                power = "0";
                break;
            default:
                power = "-";
                break;
        }
        info.Add(power);
        switch (type)
        {
            case ("Box"):
                info.Add(mass.ToString());
                break;
            default:
                info.Add("-");
                break;
        }
        switch (type)
        {
            case ("Plate"):
                info.Add(GetComponent<Plate>().force.ToString());
                break;
            default:
                info.Add("-");
                break;
        }
        string additional;
        switch (type)
        {
            case ("Energy Field"):
                additional = "This is Energy Field. It can not be moved. It can not be passed by spark. It can be turned off by floor plates";
                break;
            case ("Door"):
                additional = "This is Door. It can not be moved. It can be opened by floor plates";
                break;
            case ("Plate"):
                additional = "This is Floor plate. It can not be moved. It can be pressed by mass of " + GetComponent<Plate>().force.ToString();
                break;
            case ("PowerPlate"):
                additional = "This is power station.";
                break;
            case ("PowerBox"):
                additional = "This is generator.";
                break;
            default:
                additional = "This is " + type + ". It weights " + mass.ToString() + " and can do nothing.";
                break;
        }
        info.Add(additional);
        return info;
    }
}

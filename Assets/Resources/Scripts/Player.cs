using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Player : MonoBehaviour
{
    public bool isGhost;
    public bool isCutScened;
    public bool CanGhost = true;
    public Tilemap Walls;
    public List<Droid> droids;
    public List<Block> blocks;
    public List<Plate> plates;
    public Droid Body;
    private bool isRecon;

    public List<PlayerState> states = new List<PlayerState>();
    private CameraController _camera;
    private Sprite curSprite;
    static Player instance;


    private static Dictionary<string, int> hoverOrder = new Dictionary<string, int>
    {
        {"Droid", 0 }
        , {"Plate", 1 }
        , {"PowerPlate", 1 }
        , {"Energy Field", 2 }
        , {"Door", 3 }
        , {"Box", 4 }
    };
    public static Player GetInstance()
    {
        return instance;
    }
    
    void Start()
    {
        droids = new List<Droid>();
        foreach(Droid droid in GameObject.FindObjectsOfType<Droid>())
        {
            droids.Add(droid);
        }
        blocks = new List<Block>();
        foreach (Block block in GameObject.FindObjectsOfType<Block>())
        {
            blocks.Add(block);
        }
        foreach (Plate plate in GameObject.FindObjectsOfType<Plate>())
        {
            plates.Add(plate);
            plate.ActivatePlate(blocks);
        }
        states.Add(new PlayerState(this));
        if (instance != null)
        {
            instance.transform.position = this.transform.position;
            instance.Walls = this.Walls;
            instance.droids = this.droids;
            instance.blocks = this.blocks;
            instance.plates = this.plates;
            instance.states = this.states;
            instance.Body = this.Body;
            instance.isGhost = this.isGhost;
            Destroy(gameObject);
            instance.SecondStart();
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;

        _camera = CameraController.GetInstance();
        SecondStart();
    }

    private void SecondStart()
    {
        SwitchGhosting(isGhost);
        if (isRecon) SwitchRecon();
        UIManager.GetInstance().UpdateUI();
        if (GetComponent<Animator>() == null)
        {
            gameObject.AddComponent<Animator>();
        }
    }

    void Update()
    {
        if (!isCutScened && Input.anyKeyDown)
        {
            if (!isRecon)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    //LevelManager.GetInstance().ReloadLevel();
                    PlayerState toState = states[states.Count - 1];
                    states.Clear();
                    states.Add(toState);
                    toState.Rewind(this);
                    UIManager.GetInstance().UpdateUI();
                    return;
                }
                if (Input.GetKeyDown(KeyCode.Q) && states.Count > 1)
                {
                    PlayerState toState = states[0];
                    states.RemoveAt(0);
                    toState.Rewind(this);
                    UIManager.GetInstance().UpdateUI();
                    return;
                }
                if (Input.GetKeyDown(KeyCode.N))
                {
                    Animate();
                    LevelManager.GetInstance().LoadNextLevel();
                    return;
                }
                PlayerState curState = new PlayerState(this);
                
                bool stateChanged = Move();
                if (CanGhost && (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space)))
                {
                    stateChanged |= SwitchGhosting(!isGhost);
                }

                if (stateChanged)
                {
                    states.Insert(0, curState);
                }
            }
            if (isRecon)
            {
                Move();
            }

            UIManager.GetInstance().UpdateUI();
        }
    }

    public bool Move()
    {
        Vector3 movement = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            movement += new Vector3(0, 1, 0);
        }
        else
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            movement += new Vector3(0, -1, 0);
        }
        else
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            movement += new Vector3(1, 0, 0);
        }
        else
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            movement += new Vector3(-1, 0, 0);
        }

        if (!isRecon)
        {
            if (movement.Equals(Vector3.zero)
                || (!movement.Equals(Vector3.zero)
                 && (
                    !NoWallCollision(transform.position + movement)
                    || !NoObstaclesCollision(movement)
                )))
            {
                return false;
            }
            if (isGhost)
            {
                AudioManager.GetInstance().PlayClip("spark", .5f, 1f);
            }
            else
            {
                float pitch = 1f - (Body.mass - 1) * 0.1f;
                AudioManager.GetInstance().PlayClip("droid", pitch, pitch);
            }
            transform.position += movement;
            if (!isGhost)
            {
                Body.transform.position += movement;
            }
            foreach (Plate plate in plates)
            {
                plate.ActivatePlate(blocks);
            }
            return true;
        }
        else
        {
            if (!movement.Equals(Vector3.zero) && !NoWallCollision(transform.position + movement))
            {
                movement = Vector3.zero;
            }

            if (!movement.Equals(Vector3.zero))
            {
                transform.position += movement;
            }

            return false;
        }
    }

    private bool NoWallCollision(Vector3 checkPos) // true when no wall
    {
        return !Walls.HasTile(Vector3Int.FloorToInt(checkPos) + new Vector3Int(-1, -1, 0));
    }

    private bool NoObstaclesCollision(Vector3 direction) // true when can move
    {
        Vector3 newPosition = transform.position + direction;
        foreach (Block block in blocks)
        {
            if (newPosition == block.transform.position)
            {
                if ((isGhost && block.ghostPass)
                    || (!isGhost && block.blockPass))
                {
                    continue;
                }
                else if (isGhost && !block.ghostPass)
                {
                    return false;
                }
                else if (!isGhost && !block.blockPass)
                {
                    return block.Move(direction, blocks, Walls, Body.force, 0);
                }
            }
        }
        return true;
    }

    public void SwitchRecon()
    {
        isRecon = !isRecon;
        if (isRecon)
        {
            PlayerState curState = new PlayerState(this);
            states.Insert(0, curState);
            SwitchGhosting(true);
            curSprite = GetComponent<SpriteRenderer>().sprite;
        }
        GetComponent<SpriteRenderer>().sprite = isRecon ? Resources.Load<Sprite>("Sprites/Question") : curSprite;
        if (!isRecon)
        {
            states[0].Rewind(this);
        }
    }

    public bool SwitchGhosting(bool toGhost)
    {
        if (isGhost && !toGhost)
        {
            Body = HoverOverDroid();
            if(Body != null)
            {
                isGhost = false;
                AudioManager.GetInstance().PlayClip("powerup");
                GetComponent<SpriteRenderer>().sprite = Body.GetComponent<SpriteRenderer>().sprite;
                _camera.DroidCamera();
                return true;
            }
            else
            {
                AudioManager.GetInstance().PlayClip("zip");
                _camera.FaultyCamera(isGhost);
                return false;
            }
        }
        else if(!isGhost && toGhost)
        {
            if (CanHover())
            {
                isGhost = true;
                AudioManager.GetInstance().PlayClip("powerdown");
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Spark");
                Body = null;
                _camera.GhostCamera();
                return true;
            }
            else
            {
                AudioManager.GetInstance().PlayClip("zip");
                _camera.FaultyCamera(isGhost);
                return false;
            }
        }
        return false;
    }

    private Droid HoverOverDroid()
    {
        GameObject hovered = HoverOverGO(new string[0]);
        if (hovered != null)
        {
            Droid d = hovered.GetComponent<Droid>();
            if (d != null)
            {
                return d;
            }
        }
        return null;
    }

    private bool CanHover()
    {
        GameObject hovered = HoverOverGO(new string[] { "Droid" });
        if (hovered != null)
        {
            Block b = hovered.GetComponent<Block>();
            if (b != null)
            {
                return b.ghostPass;
            }
        }
        return true;
    }

    public List<string> HoverOverInfo(string[] exception)
    {
        List<string> info = new List<string>();
        GameObject hovered = HoverOverGO(exception);
        if (hovered != null)
        {
            Droid d = hovered.GetComponent<Droid>();
            Block b = hovered.GetComponent<Block>();
            if (d != null)
            {
                info = d.GetFullInfo();
            }
            else if (b != null)
            {
                info = b.GetFullInfo();
            }
        }
        else
        {
            info.Add("Floor");
            info.Add("Floor");
            info.Add("-");
            info.Add("-");
            info.Add("-");
            info.Add("This is floor.");
        }
        return info;
    }
    private GameObject HoverOverGO(string[] exception)
    {
        List<GameObject> list = new List<GameObject>();
        foreach(Block block in blocks)
        {
            if (block.transform.position == transform.position)
            {
                if (new List<string>(exception).Contains(block.type)) continue;
                list.Add(block.gameObject);
            }
        }
        if (list.Count == 0)
        {
            return null;
        }
        else
        {
            list.Sort((x1, x2) => (hoverOrder[x1.GetComponent<Block>().type] - hoverOrder[x2.GetComponent<Block>().type]));
            return list[0];
        }
    }

    public void Animate()
    {
        CanGhost = true;
        SwitchGhosting(true);
    }
}

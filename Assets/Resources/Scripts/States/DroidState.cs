using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DroidState
{
    public Vector3 position;
    public string id;
    public int force;

    public DroidState(Droid d)
    {
        position = d.transform.position;
        force = d.force;
        id = d.id;
    }

    public bool SameAs(Droid other)
    {
        bool result = true;
        result &= (id == other.id);
        result &= (position == other.transform.position);
        result &= (force == other.force);
        return result;
    }

    public bool SameAs(DroidState other)
    {
        bool result = true;
        result &= (id == other.id);
        result &= (position == other.position);
        result &= (force == other.force);
        return result;
    }
}

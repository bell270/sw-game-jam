using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class PlayerState
{
    public Vector3 position;
    public bool isGhost;
    public List<DroidState> droids = new List<DroidState>();
    public List<BlockState> blocks = new List<BlockState>();
    public List<PlateState> plates = new List<PlateState>();

    public PlayerState(Player player)
    {
        position = player.transform.position;
        isGhost = player.isGhost;
        foreach(Droid droid in player.droids)
        {
            droids.Add(new DroidState(droid));
        }
        foreach(Block block in player.blocks)
        {
            blocks.Add(new BlockState(block));
        }
        foreach(Plate plate in player.plates)
        {
            plates.Add(new PlateState(plate));
        }
    }

    public void Rewind(Player player)
    {
        player.transform.position = position;
        if (player.Body != null)
        {
            player.Body.transform.position = position;
        }
        for (int i = 0; i < droids.Count; i++)
        {
            player.droids[i].rewindTo(droids[i]);
        }
        for (int i = 0; i < blocks.Count; i++)
        {
            player.blocks[i].rewindTo(blocks[i]);
        }
        for (int i = 0; i < plates.Count; i++)
        {
            player.plates[i].rewindTo(plates[i]);
        }
        player.SwitchGhosting(isGhost);
    }

    public bool SameAs(PlayerState other)
    {
        if (other == null) return false;
        if (position != other.position
            || isGhost != other.isGhost
            || (droids.Count != other.droids.Count)
            || (blocks.Count != other.blocks.Count))
        {
            return false;
        }
        for (int i = 0; i < droids.Count; i++)
        {
            if (!droids[i].SameAs(other.droids[i])) { return false; }
        }
        for (int i = 0; i < blocks.Count; i++)
        {
            if (!blocks[i].SameAs(other.blocks[i])) { return false; }
        }
        for (int i = 0; i < plates.Count; i++)
        {
            if (!plates[i].SameAs(other.plates[i])) { return false; }
        }
        return true;
    }
}

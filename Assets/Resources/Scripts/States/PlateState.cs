using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlateState
{
    public Vector3 position;
    public int force;
    public bool isActive;

    public PlateState(Plate p)
    {
        force = p.force;
        isActive = p.isActive;
        position = p.transform.position;
    }

    public bool SameAs(Plate other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (force == other.force);
        result &= (isActive == other.isActive);
        result &= (position == other.transform.position);
        return result;
    }

    public bool SameAs(PlateState other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (force == other.force);
        result &= (isActive == other.isActive);
        result &= (position == other.position);
        return result;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BlockState
{
    public Vector3 position;
    public string type;
    public int mass;
    public bool ghostPass;
    public bool droidPass;

    public BlockState(Block b)
    {
        type = b.type;
        mass = b.mass;
        ghostPass = b.ghostPass;
        droidPass = b.blockPass;
        position = b.transform.position;
    }

    public bool SameAs(Block other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (type == other.type);
        result &= (mass == other.mass);
        result &= (ghostPass == other.ghostPass);
        result &= (droidPass == other.blockPass);
        result &= (position == other.transform.position);
        return result;
    }

    public bool SameAs(BlockState other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (type == other.type);
        result &= (mass == other.mass);
        result &= (ghostPass == other.ghostPass);
        result &= (droidPass == other.droidPass);
        result &= (position == other.position);
        return result;
    }
}

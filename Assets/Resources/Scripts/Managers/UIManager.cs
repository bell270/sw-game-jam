using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Main")]
    public GameObject InfoTab;
    public GameObject OptionsTab;
    public GameObject TVOff;

    [Header("InfoTab")]
    public TextMeshProUGUI levelValue;
    public TextMeshProUGUI stepsValue;
    public List<TextMeshProUGUI> infos;

    [Header("OptionsTab")]
    public TextMeshProUGUI levelNumber;
    public AudioMixer audioMixer;
    private Resolution[] resolutions;
    public TMP_Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;

    private static UIManager instance;

    public static UIManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null)
        {
            instance.InfoTab = this.InfoTab;
            instance.OptionsTab = this.OptionsTab;
            instance.levelValue = this.levelValue;
            instance.stepsValue = this.stepsValue;
            instance.infos = this.infos;
            Destroy(gameObject);
            instance.Start();
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    public void Start()
    {
        InfoTab.SetActive(true);
        OptionsTab.SetActive(false);

        levelNumber.text = LevelManager.GetInstance().curLevel.ToString();

        resolutions = Screen.resolutions;
        List<string> resolutionsList = new List<string>();
        int currentResolutionIdx = 0;
        foreach (Resolution res in resolutions)
        {
            resolutionsList.Add(res.width + "X" + res.height);
            if (res.height == Screen.height
                && res.width == Screen.width)
            {
                currentResolutionIdx = resolutionsList.Count - 1;
            }
        }
        resolutionDropdown.ClearOptions();
        resolutionDropdown.AddOptions(resolutionsList);
        resolutionDropdown.value = currentResolutionIdx;
        resolutionDropdown.RefreshShownValue();

        fullscreenToggle.isOn = Screen.fullScreen;

        float masterVolume;
        float musicVolume;
        float sfxVolume;
        audioMixer.GetFloat("masterVolume", out masterVolume);
        audioMixer.GetFloat("musicVolume", out musicVolume);
        audioMixer.GetFloat("sfxVolume", out sfxVolume);
        masterVolumeSlider.value = masterVolume;
        musicVolumeSlider.value = musicVolume;
        sfxVolumeSlider.value = sfxVolume;
    }

    public void UpdateUI()
    {
        levelValue.text = LevelManager.GetInstance().curLevel.ToString();
        stepsValue.text = (Player.GetInstance().states.Count - 1).ToString();
        List<string> info;
        if (Player.GetInstance().isGhost)
        {
            info = Player.GetInstance().HoverOverInfo(new string[0]);
            infos[0].text = "Spark";
            infos[0].color = Color.yellow;
            infos[1].text = "Hovered over";
            infos[2].color = info[0] == "Droid" ? Color.red : Color.white;
            infos[3].color = info[0] == "Droid" ? Color.red : Color.white;
        }
        else
        {
            info = Player.GetInstance().HoverOverInfo(new string[] { "Droid" });
            infos[0].text = "Droid " + Player.GetInstance().Body.id;
            infos[0].color = Color.green;
            infos[1].text = "Staying on";
            infos[2].color = Color.white;
            infos[3].color = Color.white;
        }
        infos[2].text = info[1];
        infos[3].text = info[2];
        infos[4].text = info[3];
        infos[5].text = info[4];
        infos[6].text = info[5];
    }

    public void SetMasterVolume(float volume)
    {
        audioMixer.SetFloat("masterVolume", volume);
    }

    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("musicVolume", volume);
    }

    public void SetSFXVolume(float volume)
    {
        audioMixer.SetFloat("sfxVolume", volume);
        FindObjectOfType<AudioManager>().PlayClip("click");
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        Debug.Log(QualitySettings.GetQualityLevel());
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution targetResolution = resolutions[resolutionIndex];
        Screen.SetResolution(targetResolution.width, targetResolution.height, Screen.fullScreen);
        Debug.Log(Screen.resolutions.ToString());
    }

    public void QuitGame()
    {
        StartCoroutine(QuitGameCoroutine());
    }

    IEnumerator QuitGameCoroutine()
    {
        Debug.Log("QUITTING GAME...");
        TVOff.SetActive(true);
        AudioManager am = AudioManager.GetInstance();
        am.StopAll();
        am.PlayClip("tvoff");
        PlayableDirector pd = TVOff.GetComponent<PlayableDirector>();
        pd.Play();
        while (pd.state == PlayState.Playing)
        {
            yield return null;
        }
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void SwitchRecon()
    {
        Player.GetInstance().SwitchRecon();
    }

    public void ChangeLevelNumber(int direction)
    {
        int curValue = int.Parse(levelNumber.text);
        levelNumber.text = ((LevelManager.GetInstance().openedLevels + curValue + direction) % LevelManager.GetInstance().openedLevels).ToString();
    }

    public void UnlockAllLevels()
    {
        LevelManager.GetInstance().UnlockAllLevels();
    }

    public void LoadLevel()
    {
        int curValue = int.Parse(levelNumber.text);
        LevelManager.GetInstance().LoadLevel(curValue);
        InfoTab.SetActive(true);
        OptionsTab.SetActive(false);
    }

    public void PlayClick()
    {
        AudioManager.GetInstance().PlayClip("click");
    }

    public void SetAdditionalText(string text)
    {
        infos[6].text = text;
    }

    public void SetAdditionalColor(Color color)
    {
        infos[6].color = color;
    }

    public void StartScreenSetUp(bool started)
    {
        InfoTab.SetActive(started);
        OptionsTab.SetActive(!started);
    }

    public void ShowControls()
    {
        string helpText = "Use \"?\" button to explore level."
            + "\nUse \"Q\" to rewind."
            + "\nUse \"R\" to restart level."
            + "\nPress \"Space\" to power droids up or down."
            + "\nGameJam+. Use \"N\" to skip level.";
        SetAdditionalText(helpText);
    }
}

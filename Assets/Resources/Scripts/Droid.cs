using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Droid : MonoBehaviour
{
    public int force;
    public string id;
    public int mass;

    void Awake()
    {
        Block b = gameObject.AddComponent<Block>();
        b.mass = mass;
        b.type = "Droid";
        b.ghostPass = true;
        b.blockPass = false;
        if (id == "")
        {
            id = "F" + force.ToString() + "-M" + mass.ToString();

            Sprite foundSprite = null;
            string spriteName = "droid-sized_" + ((GetComponent<Droid>().force - 1) * 3 + (mass - 1));
            Sprite[] spriteSheet = Resources.LoadAll<Sprite>("Sprites/droid/droid-sized");
            foreach (Sprite s in spriteSheet)
            {
                if (s.name.Contains(spriteName))
                {
                    foundSprite = s;
                    break;
                }
            }
            if (foundSprite != null)
            {
                GetComponent<SpriteRenderer>().sprite = foundSprite;
            }
        }

    }

    public bool CanPush(int mass)
    {
        return force >= mass;
    }

    public bool SameAs(Droid other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (id == other.id);
        result &= (transform.position == other.transform.position);
        result &= (force == other.force);
        return result;
    }

    public bool SameAs(DroidState other)
    {
        if (other == null) return false;
        bool result = true;
        result &= (id == other.id);
        result &= (transform.position == other.position);
        result &= (force == other.force);
        return result;
    }

    public void rewindTo(DroidState other)
    {
        transform.position = other.position;
        force = other.force;
    }

    public List<string> GetFullInfo()
    {
        List<string> info = new List<string>();
        info.Add("Droid");
        info.Add("Droid " + id);
        info.Add("0");
        info.Add(mass.ToString());
        info.Add(force.ToString());
        if (id == "URQT")
        {
            info.Add("Droid " + id + " is maintenance droid. It can fix anything. Generator, for example.");
        }
        else
        {
            info.Add("Droid " + id + " weights " + mass + " and can push up to " + force + " of mass.");
        }
        return info;
    }
}

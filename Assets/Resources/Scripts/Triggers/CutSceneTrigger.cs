using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Playables;

public class CutSceneTrigger : MonoBehaviour
{
    private PlayableDirector timeline;
    private Light2D global;

    public int cutSceneId = 0;
    public GameObject powerPlate;
    public GameObject powerBox;
    public Sprite sideView;
    public Sprite frontview;

    private bool textBlink;
    private float blinkTimer = 0.75f;
    private Color blinkColor = Color.white;

    private void Start()
    {
        textBlink = false;
        timeline = GetComponent<PlayableDirector>();
        global = GameObject.FindGameObjectWithTag("GlobalLight").GetComponent<Light2D>();
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!collision.gameObject.GetComponent<Player>().isGhost)
            {
                collision.gameObject.GetComponent<Player>().isCutScened = true;
                timeline.Play();
            }
        }
    }

    public void PowerOn()
    {
        global.intensity = 0.85f;
        powerPlate.GetComponent<Plate>().Switch(true);
        powerBox.GetComponent<SpriteRenderer>().material.SetFloat("_Multiplier", 1f);
        UIManager.GetInstance().UpdateUI();
        AudioManager.GetInstance().PlayClip("alarm");
        SetUiAddText();

    }
    public void PowerOff()
    {
        global.intensity = 0.7f;
        powerPlate.GetComponent<Plate>().Switch(false);
        powerBox.GetComponent<SpriteRenderer>().material.SetFloat("_Multiplier", 0f);
        UIManager.GetInstance().UpdateUI();
        AudioManager.GetInstance().StopClip("alarm");
        SetUiAddText();
    }
    public void PlayerGhosting()
    {
        Player.GetInstance().isCutScened = false;
        Player.GetInstance().CanGhost = true;
        Player.GetInstance().SwitchGhosting(true);
        powerPlate.GetComponent<Plate>().force = 2;
        textBlink = false;
    }
    public void PlayerTurningSide()
    {
        Player.GetInstance().GetComponent<SpriteRenderer>().sprite = sideView;
    }
    public void PlayerTurningFront()
    {
        Player.GetInstance().GetComponent<SpriteRenderer>().sprite = frontview;
    }
    public void CameraShake()
    {
        CameraController.GetInstance().CameraShake();
    }

    public void SetUiAddText()
    {
        string text = "";
        switch (cutSceneId) 
        {
            case (0):
                text = "WARNING!\n"
                    + "SHIP IS UNDER EMP ATTACK!\n"
                    + "DISCONNECT ALL ELECTRONIC DEVICES\n"
                    + "TO AVOID DAMAGE!\n";
                UIManager.GetInstance().SetAdditionalText(text);
                if (!textBlink)
                {
                    textBlink = true;
                    StartCoroutine(TextBlinking());
                }
                break;
            case (1):
                text = "Locating burned circuits...";
                cutSceneId++;
                UIManager.GetInstance().SetAdditionalText(text);
                break;
            case (2):
                text = "Locating burned circuits...\n"
                    + "Circuits restored";
                cutSceneId++;
                UIManager.GetInstance().SetAdditionalText(text);
                break;
            case (3):
                text = "Locating burned circuits...\n"
                    + "Circuits restored\n"
                    + "Fixing errors...";
                cutSceneId++;
                UIManager.GetInstance().SetAdditionalText(text);
                break;
            case (4):
                text = "Locating burned circuits...\n"
                    + "Circuits restored\n"
                    + "Fixing errors...\n"
                    + "Errors fixed";
                cutSceneId++;
                UIManager.GetInstance().SetAdditionalText(text);
                break;
            case (5):
                text = "Locating burned circuits...\n"
                    + "Circuits restored\n"
                    + "Fixing errors...\n"
                    + "Errors fixed\n"
                    + "Rebooting generator...";
                cutSceneId++;
                UIManager.GetInstance().SetAdditionalText(text);
                break;
            case (6):
                text = "Locating burned circuits...\n"
                    + "Circuits restored\n"
                    + "Fixing errors...\n"
                    + "Errors fixed\n"
                    + "Rebooting generator...\n"
                    + "Power restored";
                cutSceneId++;
                UIManager.GetInstance().SetAdditionalText(text);
                break;
            default:
                break;
        }
    }

    public void UpdateUI()
    {
        UIManager.GetInstance().UpdateUI();
    }

    public void PlayClick()
    {
        AudioManager.GetInstance().PlayClip("click");
    }

    public void QuitGame()
    {
        UIManager.GetInstance().QuitGame();
    }

    public void changeMusic(int type)
    {
        if (type == 0)
        {
            AudioManager.GetInstance().FadeChangeSound("carbonara", "headache", 3f, false);
            AudioManager.GetInstance().PlayClip("alarm");
        }
        else if (type == 1)
        {
            AudioManager.GetInstance().FadeChangeSound("headache", "carbonara", 1f, false);
            AudioManager.GetInstance().StopClip("alarm");
        }
    }

    IEnumerator TextBlinking()
    {
        while (textBlink)
        {
            blinkTimer -= Time.deltaTime;
            if (blinkTimer <= 0f)
            {
                blinkTimer = 0.75f;
                blinkColor = blinkColor.Equals(Color.white) ? Color.red : Color.white;
                UIManager.GetInstance().SetAdditionalColor(blinkColor);
            }
            yield return null;
        }
        UIManager.GetInstance().SetAdditionalColor(Color.white);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StartCoroutine(LoadNextLevel());
        }

        IEnumerator LoadNextLevel()
        {
            AudioManager.GetInstance().PlayClip("win");
            Player.GetInstance().Animate();
            yield return new WaitForSecondsRealtime(1f);
            LevelManager.GetInstance().LoadNextLevel();
            UIManager.GetInstance().Start();
        }
    }
}

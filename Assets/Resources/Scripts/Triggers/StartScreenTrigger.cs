using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScreenTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        UIManager.GetInstance().StartScreenSetUp(false);
    }

    public void StartGame()
    {
        LevelManager.GetInstance().LoadLevel(0);
    }
    public void PlayClick()
    {
        AudioManager.GetInstance().PlayClip("click");
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpTrigger : MonoBehaviour
{
    public int helpId;
    private bool isTriggered;
    private string helpText;
    // Start is called before the first frame update
    void Start()
    {
        switch (helpId)
        {
            case(0):
                helpText = "Use WASD or arrows to move.\n"
                    + "\nWARNING! POWER IS LOW!\n"
                    + "Objective: Get to the power station";
                break;
            case(1):
                helpText = "Use \"?\" button to explore level."
                    + "\nUse \"Q\" to rewind."
                    + "\nUse \"R\" to restart level."
                    + "\nGameJam+. Use \"N\" to skip level.";
                break;
            case(2):
                helpText = "Press \"Space\" to power droids up or down.";
                break;
            default:
                helpText = "help";
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isTriggered)
        {
            if (!Player.GetInstance().isGhost)
            {
                isTriggered = false;
            }
            UIManager.GetInstance().SetAdditionalText(helpText);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isTriggered = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isTriggered = false;
    }
}
